﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Dtos
{
    public class TopUpVehicleDto
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
