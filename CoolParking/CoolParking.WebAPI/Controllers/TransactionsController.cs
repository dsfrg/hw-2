﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Dtos;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransaction()
        {
            if(_parkingService.GetLastParkingTransactions().Length == 0)
            {
                return Ok("There are no transactions");
            }
            var lastTransaction = _parkingService.GetLastParkingTransactions().LastOrDefault();
            return Ok(JsonConvert.SerializeObject(lastTransaction));
        }

        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            if(!System.IO.File.Exists(Settings.pathToLogFile))
            {
                return NotFound("file not found");
            }

            return Ok(_parkingService.ReadFromLog());
        }

        [HttpPut("{topUpVehicle}")]
        public IActionResult TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == topUpVehicleDto.id);
            if(!Regex.IsMatch(topUpVehicleDto.id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$")  || topUpVehicleDto.Sum < 0)
            {
                return BadRequest("Wrong Request Body");
            }
            if(vehicle == null)
            {
                return NotFound("Vehickle not found");
            }
            _parkingService.TopUpVehicle(topUpVehicleDto.id, topUpVehicleDto.Sum);
            var returnedVehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == topUpVehicleDto.id);


            return Ok(JsonConvert.SerializeObject(returnedVehicle));
        }
    }
}