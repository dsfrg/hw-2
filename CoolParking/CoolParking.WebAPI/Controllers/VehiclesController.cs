﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public IActionResult GetVehicles()
        {
            var vehiclesJSON = JsonConvert.SerializeObject(_parkingService.GetVehicles());
            return Ok(vehiclesJSON);
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (!Regex.IsMatch(id.ToUpper(), @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Invalid vehicle id");
            }
            var vehicleFromRepo = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicleFromRepo == null)
            {
                return NotFound("Vehicle doesn't exist");
            }

            return Ok(JsonConvert.SerializeObject(vehicleFromRepo));
        }

        [HttpPost]
        public IActionResult InsertVehicle([FromBody] VehicleDto vehicleDto)
        {
            if (!Regex.IsMatch(vehicleDto.Id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Invalid vehicle id");
            }

            if (vehicleDto.VehicleType < 0 || vehicleDto.VehicleType > 3)
            {
                return BadRequest("Invalid VehicleType");
            }

            if (vehicleDto.Balance < 0)
            {
                return BadRequest("Balance can not be less than 0");
            }

            var vehickle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == vehicleDto.Id);
            if(vehickle != null)
            {
                return BadRequest("Current id is already exist");
            }

            _parkingService.AddVehicle(new Vehicle(vehicleDto.Id, (VehicleType)vehicleDto.VehicleType, vehicleDto.Balance));
            return Created("", vehicleDto);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Invalid vehicle id");
            }

            var vehicleFromRepo = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicleFromRepo == null)
            {
                return NotFound("Vehicle doesn't exist");
            }

            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}