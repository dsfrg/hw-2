﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;
// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logFilePath;

        public LogService(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        public string LogPath
        {
            get
            {
                return _logFilePath;
            }
        }

        public string Read()
        {
            string log = "";
            try
            {
                using (StreamReader sr = new StreamReader(_logFilePath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        log += line + "\n";
                    }
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"File not exist {e.Message}");
            }
            return log;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(_logFilePath, true))
            {
                sw.WriteLine(logInfo);
            }
        }
    }
}