﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.


namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer = null;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer = null;
        }
        public void Start()
        {
            timer = new Timer();
            timer.Interval = Interval;
            timer.Enabled = true;
            timer.Elapsed += Elapsed;
        }
        public void Stop()
        {
            timer.Stop();
        }
    }
}
