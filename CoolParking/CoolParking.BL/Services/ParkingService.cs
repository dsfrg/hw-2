﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += WithdrawEventHandler;
            //for controller working
            _withdrawTimer.Interval = Settings.withdrawTimePeriod;
            _withdrawTimer.Start();

            _logTimer.Elapsed += LogToFileEventHandler;
            //for controller working
            _logTimer.Interval = Settings.writeLogTimePeriod;
            _logTimer.Start();
        }

        private void WithdrawEventHandler(object sender, ElapsedEventArgs e)
        {
            Parking.GetInstance().ChargeFairForStaying();
        }

        private void LogToFileEventHandler(object sender, ElapsedEventArgs e)
        {
            if(GetLastParkingTransactions().Length == 0)
            {
                _logService.Write($"No transactions at this moment");
            }
            foreach (var item in GetLastParkingTransactions())
            {
                _logService.Write($"{item.transactionTime}: {item.Sum} money withdrawn from vehicle with Id='{item.vehicleId}'.\n");
            }
            Parking.GetInstance().lastTransactions.Clear();
            Parking.GetInstance().AmountForCurrentPeriod = 0;
        }


        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                throw new ArgumentException("Vechicle is null");
            }
            if (Parking.GetInstance().parking.Count == Settings.parkingCapacity)
            {
                throw new InvalidOperationException("Park is full");
            }

            var vehickeForCheck = Parking.GetInstance().parking.FirstOrDefault(x => x.Id == vehicle.Id);
            if (vehickeForCheck != null)
            {
                throw new ArgumentException("Value is already exist");
            }

            Parking.GetInstance().parking.Add(vehicle);
        }

        public void Dispose()
        {
            Parking.Reset();
        }

        public decimal GetBalance()
        {
            return (decimal)Parking.GetInstance().Amount;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - Parking.GetInstance().GetListOfCars().Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.GetInstance().lastTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.GetInstance().GetListOfCars());
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (vehicleId == null || vehicleId == "" || !Regex.IsMatch(vehicleId, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                throw new ArgumentException("Can not delete");
            }

            var vehickleForCheck = Parking.GetInstance().parking.FirstOrDefault(x => x.Id == vehicleId);
            if (vehickleForCheck == null)
            {
                throw new ArgumentException("Value is already exist");
            }
            if (vehickleForCheck.Balance < 0)
            {
                throw new InvalidOperationException("Balance is less than 0");
            }
            Parking.GetInstance().parking.Remove(vehickleForCheck);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
            {
                throw new ArgumentException();
            }
            Parking.GetInstance().GetVehicle(vehicleId).Balance += sum;
        }
    }
}
