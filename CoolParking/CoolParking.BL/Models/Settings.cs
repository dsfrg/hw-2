﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static double initialParcingBalance = 0;
        public static int parkingCapacity = 10;
        public static int withdrawTimePeriod = 5000;
        public static int writeLogTimePeriod = 60000;
        public static double fair = 2.5;
        public static Dictionary<VehicleType, decimal> SlotPrice = new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.PassengerCar, 2},
                { VehicleType.Truck, 5},
                { VehicleType.Bus, (decimal)3.5},
                { VehicleType.Motorcycle, 1}
            };
        public static string pathToLogFile = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

    }

}
