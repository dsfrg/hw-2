﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public List<Vehicle> parking;
        private static Parking _instance;
        public List<TransactionInfo> lastTransactions;

        private double amount = Settings.initialParcingBalance;
        private double amountForCurrentPeriod = 0;

        public double AmountForCurrentPeriod
        {
            get
            {
                return amountForCurrentPeriod;
            }
            set
            {
                amountForCurrentPeriod = value;
            }
        }
        public double Amount
        {
            get
            {
                return amount;
            }
            set
            {
                if (value >= 0)
                    amount += value;
            }
        }

        private Parking()
        {
            parking = new List<Vehicle>();
            lastTransactions = new List<TransactionInfo>();
        }
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }
        public List<Vehicle> GetListOfCars()
        {
            return parking;
        }
        
        public static void Reset()
        {
            _instance = new Parking();
        }
        
        public Vehicle GetVehicle(string vehicleId)
        {
            var vehickle = parking.FirstOrDefault(x => x.Id == vehicleId);
            if (vehickle == null)
            {
                throw new ArgumentException("Value is not exist");
            }
            return vehickle;
        }

        public void ChargeFairForStaying()
        {
            foreach (var item in parking)
            {
                var sum = CalculateFair(item.Balance, Settings.SlotPrice[item.VehicleType]);
                item.Balance -= sum;

                amount += (double)sum;
                amountForCurrentPeriod += (double)sum;

                this.lastTransactions.Add(new TransactionInfo() { Sum = sum, transactionTime = DateTime.Now, vehicleId = item.Id });
            }
        }

        private decimal CalculateFair(decimal currentBalance, decimal SlotPrice)
        {
            decimal result = 0;
            if (currentBalance >= SlotPrice)
            {
                result = SlotPrice;
            }
            else if( currentBalance <= 0)
            {
                result =  (SlotPrice * (decimal)Settings.fair);
            }
            else if((currentBalance - SlotPrice) < 0 )
            {
                var temp_result = currentBalance;
                temp_result += (SlotPrice - currentBalance) * (decimal)Settings.fair;
                result = temp_result;
            }
            return result;
        }
    }
}
