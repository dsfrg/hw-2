﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Schema;
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public readonly string Id;
        [JsonProperty("vehicleType")]
        public readonly VehicleType VehicleType;
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            CheckId(id);
            Id = id;

            VehicleType = vehicleType;

            CheckBalance(balance);
            Balance = balance;
        }

        private void CheckId(string testedId)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            if (!regex.IsMatch(testedId))
            {
                throw new ArgumentException("Id is not valid");
            }
        }

        public void CheckBalance(decimal testedBalance)
        {
            if (testedBalance < 0)
            {
                throw new ArgumentException("Balance is not valid");
            }
        }

        //generate id
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder generatedIdentificatorStringBuilder = new StringBuilder();
            Random random = new Random();
            int letterCount = 2;

            //generate first 2 letter
            for (int i = 0; i < letterCount; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(25 * flt));
                generatedIdentificatorStringBuilder.Append(Convert.ToChar(shift + 65));
            }
            generatedIdentificatorStringBuilder.Append("-");

            //generate literal which include 4 letter
            for (int i = 0; i < 4; i++)
            {
                generatedIdentificatorStringBuilder.Append(random.Next(0, 9));
            }
            generatedIdentificatorStringBuilder.Append("-");

            //generate last 2 letter
            for (int i = 0; i < letterCount; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(25 * flt));
                generatedIdentificatorStringBuilder.Append(Convert.ToChar(shift + 65));
            }
            return generatedIdentificatorStringBuilder.ToString();
        }
    }
}

