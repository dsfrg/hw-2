﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking
{
    public class TopUpDto
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
