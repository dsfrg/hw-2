﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking
{
    class Program
    {
        const string hostName = @"http://localhost:57619/";

        public async static Task<string> GetStringResponce(string api)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var responseBody = await httpClient.GetAsync(hostName + api);
                if (!responseBody.IsSuccessStatusCode)
                {
                    Console.WriteLine("Server is not available");
                    throw new InvalidOperationException("No responce");
                }
                return await responseBody.Content.ReadAsStringAsync();
            }
        }
        public async static Task<decimal> GetParkingBalance()
        {
            return decimal.Parse(await GetStringResponce(@"api/parking/balance")); 
        }
        public async static Task<double> GetCapacity()
        {
            return double.Parse(await GetStringResponce(@"api/parking/capacity"));
        }

        public async static Task<int> GetFreePlaces()
        {
            return int.Parse(await GetStringResponce(@"api/parking/freePlaces"));
        }

        public async static Task<List<RecivedVehicleDto>> GetVehicles()
        {
            var vehicles = JsonConvert.DeserializeObject<List<RecivedVehicleDto>>(await GetStringResponce(@"api/vehicles"));
            return vehicles;
        }


        public async static Task<RecivedVehicleDto> GetVehicleById()
        {
            Console.WriteLine("Enter id like - AA-1111-AA:");
            string id = Console.ReadLine();
            while (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                Console.WriteLine("Enter id like - AA-1111-AA:");
                id = Console.ReadLine();
            }
            var vehicles = JsonConvert.DeserializeObject<RecivedVehicleDto>(await GetStringResponce(@$"api/vehicles/{id}"));
            return vehicles;
        }

        public async static void AddVehicleToServer()
        {
            Console.WriteLine("Follow next steps to add vechicle");
            Console.WriteLine("Enter id like - AA-1111-AA:");
            string id = Console.ReadLine();
            while (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                Console.WriteLine("Enter id like - AA-1111-AA:");
                id = Console.ReadLine();
            }
            Console.WriteLine("Type of vehicle:\n" +
                               "\t0 - PassengerCar\n" +
                               "\t1 - Truck\n" +
                               "\t2 - Bus\n" +
                               "\t3 - Motorcycle");
            int vehicleType = int.Parse(Console.ReadLine());
            while (vehicleType < 1 && vehicleType > 4)
            {
                Console.WriteLine("Type of vehicle:\n " +
                               "1 - PassengerCar\n" +
                               "2 - Truck\n" +
                               "3 - Bus\n" +
                               "4 - Motorcycle");
                vehicleType = int.Parse(Console.ReadLine());
            }


            Console.WriteLine("And now car balance:");
            decimal bal = decimal.Parse(Console.ReadLine());
            while (bal <= 0)
            {
                Console.WriteLine("Car balance:");
                bal = decimal.Parse(Console.ReadLine());
            }
            var vehickle = new RecivedVehicleDto(id, vehicleType, bal);

            using (HttpClient client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(vehickle), Encoding.UTF8, "application/json");
                var response = await client.PostAsync(hostName + @"api/vehicles", content);
                if (response == null)
                {
                    throw new InvalidOperationException("Post failed");
                }
            }
        }


        public static async void DeleteVehicle()
        {
            Console.WriteLine("Enter id (AA-111-22) of car which ypu want to remove");
            string id = Console.ReadLine();
            while (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                Console.WriteLine("Enter id like - AA-1111-AA:");
                id = Console.ReadLine();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync(hostName + @$"api/vehicles/{id}");
            }
        }

        public static async Task<string> GetLastTransactions()
        {
            return await GetStringResponce(@"api/transactions/last");
        }

        public static async Task<string> GetAllTransactions()
        {
            return await GetStringResponce(@"api/transactions/all ");
        }

        //api/transactions/topUpVehicle
        public static async void TopUpVehicle()
        {
            Console.WriteLine("Enter id (AA-1111-AA) of car which you want to top up");
            string id = Console.ReadLine();
            while (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                Console.WriteLine("Enter id like - AA-1111-AA:");
                id = Console.ReadLine();
            }
            Console.WriteLine("And now sum:");
            decimal sum = decimal.Parse(Console.ReadLine());
            while (sum <= 0)
            {
                Console.WriteLine("Car balance:");
                sum = decimal.Parse(Console.ReadLine());
            }
            var topUp = new TopUpDto();
            topUp.id = id;
            topUp.Sum = sum;

            using (HttpClient client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(topUp), Encoding.UTF8, "application/json");
                var response = await client.PutAsync(hostName + @"api/transactions/topUpVehicle", content);
                if (response == null)
                {
                    throw new InvalidOperationException("Put failed");
                }
            }

        }

        public static void ShowVehickles()
        {
            if (GetVehicles().Result.Count == 0)
            {
                Console.WriteLine("There are no vehicles yet");
                continueConfirmation();
                return;
            }
            Console.WriteLine("All vechicles: ");

            foreach (var item in GetVehicles().Result)
            {
                Console.WriteLine($"\tId: {item.Id}, Type: {item.VehicleType}, Sum: {item.Balance}");
            }
        }
        static void Main(string[] args)
        {
            int userChoise = showMenu();
            while (userChoise != 0)
            {
                Console.Clear();
                switch (userChoise)
                {
                    case 1:
                        {
                            Console.WriteLine($"Parking balance: {GetParkingBalance().Result}");
                            continueConfirmation();
                            break;
                        }

                    case 2:
                        {
                            Console.WriteLine($"Parking capacity: {GetCapacity().Result}");
                            continueConfirmation();
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine($"Free space {GetFreePlaces().Result} from {GetCapacity().Result}");
                            continueConfirmation();
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine(GetLastTransactions().Result);
                            continueConfirmation();
                            break;
                        }
                    case 5:
                        {
                            Console.WriteLine(GetAllTransactions().Result);
                            continueConfirmation();
                            break;
                        }
                    case 6:
                        {
                            ShowVehickles();
                            continueConfirmation();
                            break;
                        }
                    case 7:
                        {
                            AddVehicleToServer();
                            continueConfirmation();
                            break;
                        }
                    case 8:
                        {
                            DeleteVehicle();
                            continueConfirmation();
                            break;
                        }
                    case 9:
                        {
                            TopUpVehicle();
                            continueConfirmation();
                            break;
                        }
                    case 10:
                        {
                            var vehickle = GetVehicleById().Result;
                            Console.WriteLine($"Id: {vehickle.Id}, Vehicle type: {vehickle.VehicleType}, Balance: {vehickle.Balance}");
                            continueConfirmation();
                            break;
                        }
                }
                userChoise = showMenu();
            }

            //Console.Read();

        }

        public static int showMenu()
        {
            Console.WriteLine(
                "1 -> Show parking balance\n" +
                "2 -> Show parking capacity\n" +
                "3 -> Show number of free space\n" +
                "4 -> Show last transaction\n" +
                "5 -> Show transaction history\n" +
                "6 -> Show all cars on parking\n" +
                "7 -> Park the car\n" +
                "8 -> Remove the car\n" +
                "9 -> Top up the balance of the car(by id)\n" +
                "10 -> Show vehickle by id\n" +
                "0 -> Exit"
                );
            Console.Write("Your choice: ");
            Int32.TryParse(Console.ReadLine(), out int user_choise);
            return user_choise;
        }

        static public void continueConfirmation()
        {
            Console.WriteLine("Would you like to continue?(Y/N)?");
            string answer = Console.ReadLine();

            if (answer == "Y" || answer == "y")
            {
                Console.Clear();
            }
            else
            {
                Console.WriteLine("See you soon! Cood buy!");
                System.Environment.Exit(0);
            }
        }
    }
}
