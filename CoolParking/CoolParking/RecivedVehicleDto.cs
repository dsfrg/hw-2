﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking
{
    public class RecivedVehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public RecivedVehicleDto(string id, int vehicleType, decimal bal)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = bal;
        }
    }
}
